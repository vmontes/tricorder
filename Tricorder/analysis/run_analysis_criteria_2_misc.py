
import os
import shutil
import glob
import ntpath
import time
import sys

def create_folders(working_path, mutant_name, workload_type, ftype):

    tpath = working_path
    try:
        os.mkdir(tpath)
    except:
        print 'path exists:' + tpath

    tpath = tpath + '/' + mutant_name + '_' + ftype
    try:
        os.mkdir(tpath)
    except:
        print 'path exists:' + tpath

    result_path = tpath

    tpath = tpath + '/' +workload_type
    try:
        os.mkdir(tpath)
    except:
        print 'path exists:' + tpath
        shutil.rmtree(tpath)
        os.mkdir(tpath)

    return tpath, result_path

def copy_ref_data(data_path, target_path, ftype):
    base_files = glob.glob(data_path + '/*Server_exe*_*'+ftype)
    for f in base_files:
        name = ntpath.basename(f)
        shutil.copy(f,target_path+'/'+name)

def run_test(data_path, 
                mutant_name, 
                workload_type, 
                ftype, 
                target_path, 
                python_path, 
                damicore_path, 
                results_path):

    tfiles = glob.glob(data_path + '/' + '*_' +mutant_name + '*' + ftype)
    work_count = 0
    for i,tfile in enumerate(tfiles,1):
        starttime = time.time()
        testtag = workload_type + '_' + str(i)
        outfile = results_path + '/' + testtag + '.out'
        name = ntpath.basename(tfile)
        shutil.copy(tfile,target_path+'/'+name)
        if ntpath.isfile(outfile) == False:
            print 'working on: ' + target_path
            
            cmd = python_path + ' ' +  damicore_path + ' --compressor ppmd --model-order 12 --memory 100'\
            + ' --tree-output ' + results_path + '/' + testtag + '.newick'   \
            + ' -o ' + outfile \
            + ' ' + target_path

            print 'cmd : ' + cmd

            os.system(cmd)

            if ntpath.isfile(outfile) == True:
                with open(outfile, 'r') as f:
                    m = dict()
                    for line in f.readlines():
                        line = [d.strip() for d in line.split(',')]
                        v = int(line[1])
                        if v in m:
                            m[v].append(line[0])
                        else:
                            m[v] = [line[0]]

                    work = any([all(mutant_name in item for item in l) for l in m.values()])

                    if work == True:
                        work_count = work_count + 1
                        print 'WORKED - doing again'
                        #cmd = python_path + ' ' +  damicore_path + ' -c zlib' \
                        cmd = python_path + ' ' +  damicore_path + ' --compressor ppmd --model-order 12 --memory 100'\
                        + ' --tree-output ' + results_path + '/' + testtag + '.newick'   \
                        + ' -o ' + outfile +'.worked' + str(work_count) \
                        + ' ' + target_path

                        print 'cmd : ' + cmd

                        os.system(cmd)
                        print 'WORKED elapsed : ' + str(time.time() - starttime) + ' secs'
                        if work_count == 2:
                            break
                    else:
                        print 'NOT WORKED elapsed : ' + str(time.time() - starttime) + ' secs'
                        work_count = 0
                i = i  + 1
        else:
            print 'Results exists: ' + outfile
            if ntpath.isfile(outfile+'.worked2') == True:
                break
          
#wokloads = ['BINCSVL1','BINCSVL2','BINCSVL3','BINCSVM1','BINCSVM2','BINCSVM3','BINCSVH1','BINCSVH2','BINCSVH3','BINL1','BINM1','BINH1','CSVL1','CSVM1','CSVH1']
wokloads = ['misc']
defects = ['bclean','infinite','mono','nobreak','sleep','swap', 'unlock']


working_path = '/home/vitor/python/tricorder_data/measurements/workspace/misc'
test_data_path = '/home/vitor/python/tricorder_data/measurements/data_misc_csv'
ref_data_path = '/home/vitor/python/tricorder_data/measurements/data_csv'
python_path = '/home/vitor/anaconda2/bin/python'
damicore_path = '/home/vitor/python/damicorepy/damicore/damicore.py'
file_type = 'csv'

for d in defects:
    for w in wokloads:
        target_path, result_path = create_folders(working_path,d,w,file_type)
        copy_ref_data(ref_data_path,target_path,file_type)
        run_test(test_data_path,d,w,file_type,target_path,python_path,damicore_path, result_path)
