import glob
import os
import collections

def get_results(path):
    r = {}
    tfiles = glob.glob(path + '/*worked1')
    for f in tfiles:
        names = os.path.basename(f).split('_')
        w = names[0]
        num = int(names[1].split('.')[0])
        if w in r:
            if num < r[w]:
                r[w] = num
        else:
            r[w] = num
    return r


def show_results(path):
    print('-------------------------------------------------------')
    print(os.path.basename(path))
    print('-------------------------------------------------------')
    r = get_results(path)
    for k,v in collections.OrderedDict(sorted(r.items())).items():
        print(k+':'+str(v))
    print('-------------------------------------------------------')


show_results('/home/vitor/python/tricorder_data/measurements/workspace/bclean_csv')
show_results('/home/vitor/python/tricorder_data/results/restrictive/bclean')

show_results('/home/vitor/python/tricorder_data/measurements/workspace/mono_csv')
show_results('/home/vitor/python/tricorder_data/results/restrictive/mono')

show_results('/home/vitor/python/tricorder_data/measurements/workspace/sleep_csv')
show_results('/home/vitor/python/tricorder_data/results/restrictive/sleep')

show_results('/home/vitor/python/tricorder_data/measurements/workspace/swap_csv')
show_results('/home/vitor/python/tricorder_data/results/restrictive/swap')


show_results('/home/vitor/python/tricorder_data/measurements/workspace/infinite_csv')
show_results('/home/vitor/python/tricorder_data/results/restrictive/infinite')

show_results('/home/vitor/python/tricorder_data/measurements/workspace/nobreak_csv')
show_results('/home/vitor/python/tricorder_data/results/restrictive/nobreak')

show_results('/home/vitor/python/tricorder_data/measurements/workspace/unlock_csv')
show_results('/home/vitor/python/tricorder_data/results/restrictive/unlock')