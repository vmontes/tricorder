import sys
print ('Running Inspector Gadget version 02/2019')
print (sys.version)

import time
import glob
import os
import subprocess
from monitor import monitor
from os import listdir
from os.path import isfile, join
import shutil
import json
import hashlib

def create_output_dir(workingdir):
	csvdir = 'raw'
	curdate = time.strftime("%d_%m_%Y")
	curtime = time.strftime("%H%M%S")
	dir = workingdir + '/' + str(curdate)
	
	try:
		os.mkdir(dir)
	except:
		print ('already created dir ' + dir)

	csvdir = dir + '/raw'
	try:
		os.mkdir(csvdir)
	except:
		print ('already created dir ' + csvdir)

	prefix = str(curdate)+'_'+str(curtime)
	return csvdir,prefix

# import matplotlib.pyplot as plt
# print(plt.gcf().canvas.get_supported_filetypes())

# EXPERIMENT CONFIGURATION #
interval = 0.1
samples = 300
executions = 30
input_type = 'network' # example: mp3 or lib or whatever

# INTERNAL CONFIGURATION #

xpath = '/home/vitor/temp' + input_type 
print('experiment path: ' + xpath)

bpath = 'c:/datalab/' + input_type + '/bin'
print('bin path: '+ bpath)

exefiles = glob.glob(bpath+"/yatServer*.exe")

configfiles = glob.glob('c:/datalab/network/defs/*.json')
config_file_all = xpath + '/defs/defs_all.json' 
config_file = xpath + '/defs.json' 

output_path,prefix = create_output_dir(xpath)

with open(config_file_all, 'r') as jsonFile:
	jvalues = json.load(jsonFile)
	jsonFile.close()

shutil.copyfile(config_file_all,output_path+'/defs_all.json')

for jcon in jvalues['configurations']:

	experiment_tag = jcon['tag']
	
	prefix = experiment_tag

	print("Configuration: "+ prefix)

	with open(config_file, 'w') as jsonFile:
		json.dump(jcon,jsonFile)
		jsonFile.close()

	shutil.copyfile(config_file,output_path+'/'+prefix+'defs.json')

	prepare_exe = bpath+'/PrepareWorkload.exe'
	proc_prepare = subprocess.Popen([prepare_exe,config_file])
	proc_prepare.wait()

	for sut_server in exefiles:
		sut_server = sut_server.replace('\\','/')
		sut_server_file = os.path.basename(sut_server)
		sut_client = bpath + '/yatClient.exe'
		working_path = xpath

		print ('working path: ' + working_path)
		print ('SUTs: ' + sut_server + ' ' + sut_client)

		for i in range(executions):
			print ("execution " + str(i))
			proc_server = subprocess.Popen([sut_server,config_file])
			m = monitor(sut_server_file,prefix,output_path,interval,samples,False,True)
			proc_client = subprocess.Popen([sut_client,config_file])
			m.get_samples()
			m.dump_csv(i)
			proc_server.kill()
			proc_client.kill()
			proc_client.wait()
			proc_server.wait()

			BLOCKSIZE = 65536
			bin_hash = ''
			csv_hash = ''

			hasher = hashlib.sha1()
			try:
				with open(working_path + '/output/bin/'+sut_server_file+'.dump.bin', 'rb') as afile:
					buf = afile.read(BLOCKSIZE)
					while len(buf) > 0:
						hasher.update(buf)
						buf = afile.read(BLOCKSIZE)
				bin_hash = hasher.hexdigest()
				print(bin_hash)
			except:
				print ('no bin output')

			hasher = hashlib.sha1()
			try:
				with open(working_path + '/output/csv/'+sut_server_file+'42.dump.csv', 'rb') as afile:
					buf = afile.read(BLOCKSIZE)
					while len(buf) > 0:
						hasher.update(buf)
						buf = afile.read(BLOCKSIZE)
				csv_hash = hasher.hexdigest()
				print(csv_hash)
			except:
				print ('no csv output')

			with open(output_path + '/' + prefix + '.bin.hash', 'a') as afile:
				afile.write(sut_server_file+':\t'+bin_hash+'\n')
			with open(output_path + '/' + prefix + '42.csv.hash', 'a') as afile:
				afile.write(sut_server_file+':\t'+csv_hash+'\n')

			print ("done execution " + str(i))
	print("Finished config file")
print("Finished")

