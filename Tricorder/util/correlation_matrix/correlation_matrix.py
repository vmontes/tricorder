from sklearn.datasets import load_breast_cancer
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import glob
import os
import math
import csv  

############################################################################
# CONFIGURATION #
############################################################################
# DEFAULT #
header_list = ["CPU", "RAM", "Acessos(IO)", "Bytes(IO)", "GPU", "VRAM"]
input_data = 'data'
test_mode = False
############################################################################
# TEST #
#header_list = ['sinx', 'sin2x', '2sinx', 'cosx', 'cos3x', 'sinx^2', '3cosx']
#input_data = 'test_data'
#test_mode = True
############################################################################

def generate_test_data():
	
	data = []
	for i in range(0,100):
		line = []
		line.append(math.sin(i))
		line.append(math.sin(2*i))
		line.append(2*math.sin(i))
		line.append(math.cos(i))
		line.append(math.cos(3*i))
		line.append(math.sin(i*i))
		line.append(3*math.cos(i))
		data.append(line)

	try:
		os.mkdir('test_data')
	except:
		print('test_data already created')
	
	with open('test_data/sample1.csv', 'w', encoding='UTF8') as f:
		writer = csv.writer(f, delimiter=';')
		#writer.writerow(names)
		writer.writerows(data)


def create_correlation_matrix(inputpath, file, outpath):
	print('creating correlation matrix for ' + file)
	
	df = pd.read_csv(inputpath +'/'+file, delimiter=';', names=header_list)

	print(df.describe())

	correlation_mat = df.corr()
	sns.heatmap(correlation_mat, annot = True)
	plt.savefig(outpath+'/'+file+".png")
	plt.close()

def run_correlation_at_folder(input_path):
	outpath = 'img'
	try:
		os.mkdir(outpath)
	except:
		print(outpath+' already created') 

	files = glob.glob(input_path+'/*csv')
	for f in files:
		filename = os.path.basename(f)
		create_correlation_matrix(input_path, filename, outpath)

def run_correlation_once(input_path):
	outpath = 'test'
	try:
		os.mkdir(outpath)
	except:
		print(outpath+' already created') 
	create_correlation_matrix(input_path,'/SEC_MEDIUM_demo_cli_py_44_csv', outpath)

if test_mode == True:
	generate_test_data()

run_correlation_at_folder(input_data)

print("this is the end")