
from tricorder_plot import tricorder_plot
import glob
import os

################################################################################
# CONFIGURATION
################################################################################
data_path = 'D:\\tricorder\\tricorder_data\\measurements\\data_csv'
results_path = 'D:\\tricorder\\tricorder_data\\results\\standard'
defects = ['bclean','infinte','mono', 'sleep', 'nobreak', 'swap', 'unlock', 'control']
################################################################################

################################################################################
# GRAPHS PATH
cur_path = os.getcwd()
img_path = cur_path + '\\img_small'
if os.path.isdir(img_path) == False:
    os.mkdir(img_path)
################################################################################

for  defect in defects:
    path = results_path + '\\' + defect + '\\*.worked'

    workloads = []
    files = []
    for name in glob.glob(path):
        init = name.rfind('\\')
        file = name[init+1:]
        workload = file.split('_')[0]
        if workload not in workloads:
            print('added: ' + file)
            workloads.append(workload)
            files.append(file)

    for file in files:
        print('working on: ' + file)
        tp = tricorder_plot(data_path, results_path, defect, file, save_plot_path=img_path, save_plot=True, show_plot=False)
        tp.create_plot()
        print('done')

print('all done')