""" import sys
from PIL import Image

base_path = "C:/datalab/network/06_02_2019/infinite/png_0/"

images = map(Image.open, [base_path+'BINCSVyatServer_exe_0.png',base_path+'BINCSVyatServer_exe_1.png'])
widths, heights = zip(*(i.size for i in images))

total_width = sum(widths)
max_height = max(heights)

new_im = Image.new('RGB', (total_width, max_height))

x_offset = 0
for im in images:
  new_im.paste(im, (x_offset,0))
  x_offset += im.size[0]

new_im.save(base_path+'test.png') """



from __future__ import print_function
import os
import glob

from PIL import Image

base_path = 'C:/datalab/network/results/png/test_all'

files = glob.glob(base_path+"/*.png")

for index, file in enumerate(files):
	img = Image.open(file)
	file_out = file+'.jpg'
	img.save(file_out)

result = Image.new("RGB", (len(files)*640, 480))

for index, file in enumerate(files):
  path = os.path.expanduser(file)
  img = Image.open(path)
  img.thumbnail((640, 480), Image.ANTIALIAS)
  x = index * 640
  y = 0
  w, h = img.size
  print('pos {0},{1} size {2},{3}'.format(x, y, w, h))
  result.paste(img, (x, y, x + w, y + h))

result.save(os.path.expanduser(base_path+'/all.png'))
