import matplotlib.pyplot as plt
import csv
import ntpath
import statistics 
import numpy as np

class tricorder_plot:
    def __init__(self,data_path, results_path, defect, target_results_file, save_plot=True, save_plot_path='', show_plot=True):
        self.data_path = data_path
        self.results_path = results_path
        self.defect = defect
        self.target_results_file = target_results_file
        self.save_plot = save_plot
        self.show_plot = show_plot
        self.save_plot_path = save_plot_path
        self.init_paths()

    def init_paths(self):
        self.outfile = self.results_path + '\\' + self.defect + '\\' + self.target_results_file
        self.workload = self.target_results_file.split('_')[0]
        self.reference_prefix = self.workload + '_yatServer_exe'

    # read the csv results file
    def read_file(self, csvfile):
        pad = 1
        x = list(range(-1*pad,0))
        cpu = [0] * pad
        mem = [0] * pad
        io = [0] * pad
        count = 0
        for row in csvfile:
            if len(row) > 0:
                x.append(count)
                count = count + 1
                cpu.append(float(row[0]))
                mem.append(float(row[1]))
                io.append(float(row[2]))
        return x,cpu,mem,io

    # plot all the values at a same graph
    def plot_values(self, xvalues, yvalues, ylabel, names, workload, defect, title):
        #fig = plt.figure(figsize=((10.0,4.0)))
        fig = plt.figure()
        for name,y in sorted(zip(names,yvalues)):            
            fig.add_subplot().plot(xvalues,y, label=name)
        fig.add_subplot().set_xlabel('time (ms)')
        fig.add_subplot().set_ylabel(ylabel)
        fig.add_subplot().set_title(title+' with '+'workload: '+workload+' and '+'defect: '+defect)
        fig.add_subplot().legend(loc='upper right', bbox_to_anchor=(1.15, 1.0))
        if self.save_plot == True:
            fig.savefig(self.save_plot_path + '\\' + title+' '+workload+' '+defect+'.png', bbox_inches='tight')
        if self.show_plot == True:
            plt.show()
        plt.close(fig)

    # plot all the values at a same graph
    def plot_avg(self, xvalues, yvalues, ylabel, names, workload, defect, title):
        yavg = []
        ymed = []
        ymax = []
        ymin = []
        for v in zip(*yvalues):
            yavg.append(sum(v) / len(v))
            ymax.append(max(v))
            ymin.append(min(v))
            ymed.append(np.median(v))
        #fig = plt.figure(figsize=((10.0,4.0)))
        fig = plt.figure()
        fig.add_subplot().plot(xvalues, yavg, label='avg')
        fig.add_subplot().plot(xvalues, ymed, label='median')
        fig.add_subplot().plot(xvalues, ymax, label='max')
        fig.add_subplot().plot(xvalues, ymin, label='min')
        fig.add_subplot().set_xlabel('time (ms)')
        fig.add_subplot().set_ylabel(ylabel)
        fig.add_subplot().set_title(title+' with '+'workload: '+workload+' and '+'defect: '+defect)
        fig.add_subplot().legend(loc='upper right', bbox_to_anchor=(1.14, 1.0))
        if self.save_plot == True:
            fig.savefig(self.save_plot_path + '\\avg' + title+' '+workload+' '+defect+'.png')
        if self.show_plot == True:
            plt.show()
        plt.close(fig)
        

    def create_plot(self):
        # open the results file
        if ntpath.isfile(self.outfile) == True:
            with open(self.outfile, 'r') as f:
                m = dict()

                # create lists for each cluster
                for line in f.readlines():
                    line = [d.strip() for d in line.split(',')]
                    v = int(line[1])
                    if v in m:
                        m[v].append(line[0])
                    else:
                        m[v] = [line[0]]

                # test data handle: tricorder cluster
                list_x = []
                list_cpu = []
                list_mem = []
                list_io = []
                list_names = []
                for group in m.values():
                    # find the Tricorder criteria cluster
                    found = any([all(self.defect in item for item in group)])
                    if found:
                        for f in group:
                            # open the each measure data file refered at the cluster
                            datafilepath = self.data_path + '\\' + f
                            with open(datafilepath) as csvfile:
                                spamreader = csv.reader(csvfile, delimiter=';')
                                x,cpu,mem,io = self.read_file(spamreader)
                                x = [tmp * 100 for tmp in x] # convert to ms
                                cpu = [tmp/ 4.0 for tmp in cpu] # 400% CPU usage
                                mem = [tmp/ 1e9 for tmp in mem] # GB usage
                                list_x.append(x) 
                                list_cpu.append(cpu) 
                                list_mem.append(mem)
                                list_io.append(io)
                                # start index at 0, but showing numbers at 01 (with 2 digits)
                                nindex = int(str(f.split('_')[4]).zfill(2))
                                nameindex = str(nindex+1).zfill(2) + ' exec'
                                list_names.insert(nindex, nameindex)

                # reference data handle
                ref_x = []
                ref_cpu = []
                ref_mem = []
                ref_io = []
                ref_names = []
                for group in m.values():            
                    for f in group:
                        # find the reference measures for target workload
                        found = self.reference_prefix in f
                        if found:
                            datafilepath = self.data_path + '\\' + f
                            # open each measure data file
                            with open(datafilepath) as csvfile:
                                spamreader = csv.reader(csvfile, delimiter=';')
                                x,cpu,mem,io = self.read_file(spamreader)
                                x = [tmp * 100 for tmp in x] # convert to ms
                                cpu = [tmp/ 4.0 for tmp in cpu] # 400% CPU usage
                                mem = [tmp/ 1e9 for tmp in mem] # GB usage
                                ref_x.append(x)
                                ref_cpu.append(cpu)
                                ref_mem.append(mem)
                                ref_io.append(io)
                                 # start index at 0, but showing numbers at 01 (with 2 digits)
                                nindex = int(str(f.split('_')[3]).zfill(2))
                                nameindex = str(nindex+1).zfill(2) + ' exec'
                                ref_names.insert(nindex, nameindex)

                # create a average values list for the 30 reference measures
                #fmean_ref_cpu = list(map(statistics.fmean, zip(*ref_cpu)))
                #mean_ref_cpu = list(map(statistics.mean, zip(*ref_cpu)))
                #median_ref_cpu = list(map(statistics.median, zip(*ref_cpu)))
                #low_ref_cpu = list(map(statistics.median_low, zip(*ref_cpu)))
                #high_ref_cpu = list(map(statistics.median_high, zip(*ref_cpu)))
                #mean_ref_mem = list(map(statistics.fmean, zip(*ref_mem)))
                #mean_ref_io = list(map(statistics.fmean, zip(*ref_io)))

                #########################################################################################
                # plots completos das referências com a média (sem dados de teste)
                self.plot_avg(ref_x[0], list_cpu, 'CPU usage (%)', list_names, self.workload, self.defect, 'CPU usage')
                self.plot_avg(ref_x[0], list_mem, 'RAM usage (GB)', list_names, self.workload, self.defect, 'RAM usage')
                self.plot_avg(ref_x[0], list_io, 'IO read count', list_names, self.workload, self.defect, 'IO usage')
                #########################################################################################

                #########################################################################################
                # plots completos das referências com a média (sem dados de teste)
                self.plot_avg(ref_x[0], ref_cpu, 'CPU usage (%)', ref_names, self.workload, 'no defect, training data', 'CPU usage')
                self.plot_avg(ref_x[0], ref_mem, 'RAM usage (GB)', ref_names, self.workload, 'no defect, training data', 'RAM usage')
                self.plot_avg(ref_x[0], ref_io, 'IO read count', ref_names, self.workload, 'no defect, training data', 'IO usage')
                #########################################################################################


                #########################################################################################
                # plots do cluster que atendeu ao crítério Tricorder, com a média das referências
                self.plot_values(ref_x[0], list_cpu, 'CPU usage (%)', list_names, self.workload, self.defect, 'CPU usage')
                self.plot_values(ref_x[0], list_mem, 'RAM usage (GB)', list_names, self.workload, self.defect, 'RAM usage')
                self.plot_values(ref_x[0], list_io, 'IO read count', list_names, self.workload, self.defect, 'IO usage')
                #########################################################################################

                #########################################################################################
                # plots completos das referências com a média (sem dados de teste)
                self.plot_values(ref_x[0], ref_cpu, 'CPU usage (%)', ref_names, self.workload, 'no defect, training data', 'CPU usage')
                self.plot_values(ref_x[0], ref_mem, 'RAM usage (GB)', ref_names, self.workload, 'no defect, training data', 'RAM usage')
                self.plot_values(ref_x[0], ref_io, 'IO read count', ref_names, self.workload, 'no defect, training data', 'IO usage')
                #########################################################################################

               
                

       
        